//
//  SelectPokemonTypeTableViewCell.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit
import SDWebImage

protocol SelectPokemonTypeTableViewCellDelegate {
    func didSelectPokemonType(selectedType: String)
}

class SelectPokemonTypeTableViewCell: UITableViewCell {

    
   
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var radioButton: UIButton!
    
    var delegate: SelectPokemonTypeTableViewCellDelegate?
    var typeName: String?
    
    var isRadioButtonSelected: Bool = false {
        didSet{
            radioButton.setImage(UIImage(named: isRadioButtonSelected ? "radio-on" : "radio-off"), for: .normal)
        }
    }
    
    var pokemonType: PokemonType? {
        didSet{
            guard let type = pokemonType else { return }
            typeImage.sd_setImage(with: URL(string: type.thumbnailImage), placeholderImage: nil)
            typeLabel.text = type.name.capitalized
            typeName = type.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .white
    }
    
    @IBAction func radioButtonTouched(_ sender: Any) {
        isRadioButtonSelected = isRadioButtonSelected ? false : true
        guard let type = typeName else { return }
        delegate?.didSelectPokemonType(selectedType: type)
    }
}
