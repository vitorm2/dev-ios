//
//  MainButton.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

@IBDesignable
class MainButton: UIButton {

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    
    func setup() {
        self.layer.cornerRadius = 10
        self.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.1137254902, blue: 0.462745098, alpha: 1)
        self.tintColor = .white
        layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.15).cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 1.0
        layer.masksToBounds = false
        titleLabel?.font = UIFont.systemFont(ofSize: 22)
    }

}

@IBDesignable
class SmallTextButton: MainButton {
    
    override func setup() {
        super.setup()
        titleLabel?.font = UIFont.systemFont(ofSize: 16)
    }
}


