//
//  CustomLabel.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

class BaseLabel: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        textColor = .white
        font = UIFont.systemFont(ofSize: 20, weight: .medium)
    }
}


@IBDesignable
class TitleLabel: BaseLabel {
    
    override func setup() {
        super.setup()
        textColor = .white
        font = UIFont.systemFont(ofSize: 26, weight: .medium)
    }
}

@IBDesignable
class DescriptionLabel: BaseLabel {
    
    override func setup() {
        super.setup()
    }
}
