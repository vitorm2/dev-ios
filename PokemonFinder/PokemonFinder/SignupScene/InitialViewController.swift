//
//  InitialViewController.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

class InitialViewController: UIViewController {
    
    let coordinator: MainCoordinator

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = true
    }
    
    @IBAction func letsGoButtonTouched(_ sender: Any) {
        self.coordinator.goToSignupScreen(signupStage: .registerName, viewModel: nil)
    }
    

    init(coordinator: MainCoordinator) {
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
