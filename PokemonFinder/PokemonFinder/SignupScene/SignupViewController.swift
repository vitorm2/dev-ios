//
//  SignupViewController.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

enum SignupStageType {
    case registerName
    case selectFavoriteType
}

class SignupViewController: UIViewController {
    
    @IBOutlet weak var backArrowButton: UIButton!
    @IBOutlet weak var topTitle: TitleLabel!
    @IBOutlet weak var customTextFieldView: CustomTextFieldView!
    
    let selectPokemonTypeView = SelectPokemonTypeView()
    
    let coordinator: MainCoordinator
    let singupStage: SignupStageType
    
    let viewModel: SignupViewModel

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true

        viewModel.delegate = self
        hideKeyboardWhenTappedAround()
        setupUI()
    }
    
    init(coordinator: MainCoordinator, signupStage: SignupStageType, viewModel: SignupViewModel) {
        self.coordinator = coordinator
        self.singupStage = signupStage
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI(){
        switch singupStage {
        case .registerName:
            topTitle.text = "Let's me each other first?"
            customTextFieldView.titleLabel.text = "First we need to know your name..."
        case .selectFavoriteType:
            guard let userName = viewModel.userName else { return }
            topTitle.text = "Hello, trainer \(userName)"
            customTextFieldView.titleLabel.text = "...now tell us wich is your favorite Pokémon type:"
            customTextFieldView.isPokemonTypeInput = true
            customTextFieldView.delegate = self
            backArrowButton.isHidden = false
        }
    }
    
    @IBAction func nextButtonTouched(_ sender: Any) {
        switch singupStage {
        case .registerName:
            guard let name = customTextFieldView.mainTextField.text else { return }
            if name.isEmpty {
                present(message: "Campo de nome vazio!")
                return
            }
            viewModel.userName = name
            goToSelectedFavoriteTypeStage()
        case .selectFavoriteType:
            viewModel.registerUser()
        }
        
    }
    
    func goToSelectedFavoriteTypeStage(){
        coordinator.goToSignupScreen(signupStage: .selectFavoriteType, viewModel: viewModel)
    }
    
    @IBAction func backArrowButtonTouched(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    

}

extension SignupViewController: CustomTextFieldViewDelegate {
    func showPokemonTypesActionSheet() {
        viewModel.fetchPokemonTypes()
    }
}

extension SignupViewController: SelectPokemonTypeViewDelegate {
    
    func didSelectType(type: String) {
        selectPokemonTypeView.removeFromSuperview()
        viewModel.selectedPokemonType = type
        customTextFieldView.mainTextField.text = type.capitalized
    }
    
    func showAlert(message: String) {
        present(message: message)
    }
}

extension SignupViewController: SignupViewModelDelegate {
    
    func didGetPokemonTypes(types: [PokemonType]) {
        DispatchQueue.main.sync {
            selectPokemonTypeView.delegate = self
            selectPokemonTypeView.constraintFully(to: view)
            selectPokemonTypeView.pokemonTypes = types
            
            view.addSubview(selectPokemonTypeView)
        }
    }
    
    func didRegisterUser() {
        coordinator.goToHomeScreen()
    }
    
    func failed(message: String) {
        present(message: message)
    }
}
