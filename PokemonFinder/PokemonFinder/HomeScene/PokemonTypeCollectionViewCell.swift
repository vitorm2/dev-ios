//
//  PokemonTypeCollectionViewCell.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 15/11/20.
//

import UIKit
import SDWebImage

class PokemonTypeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    
    var pokemonType: PokemonType? {
        didSet{
            guard let pokemonType = self.pokemonType else { return }
            typeImage.sd_setImage(with: URL(string: pokemonType.thumbnailImage), placeholderImage: nil)
            typeLabel.text = pokemonType.name.capitalized
        }
    }
    
    var isSelectedType: Bool = false {
        didSet{
            typeLabel.textColor = isSelectedType ? #colorLiteral(red: 0.0862745098, green: 0.7803921569, blue: 0.631372549, alpha: 1) : .black
            typeLabel.font = isSelectedType ? UIFont.systemFont(ofSize: 19, weight: .bold) : UIFont.systemFont(ofSize: 19, weight: .medium)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .white
    }

}
