//
//  HomeViewController.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 14/11/20.
//

import UIKit

class HomeViewController: LoadingViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    var editBarButton = UIBarButtonItem()
    var searchBarButton = UIBarButtonItem()
    var cancelBarButton = UIBarButtonItem()
    var searchBar = UISearchBar()
    let selectPokemonTypeView = SelectPokemonTypeView()
    
    let coordinator: MainCoordinator
    let viewModel: HomeViewModel
    
    var selectedType: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.title  = "Pokémon Finder"
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.0862745098, green: 0.7803921569, blue: 0.631372549, alpha: 1)
        navigationController?.navigationBar.isHidden = false
        
        viewModel.delegate = self
        setup()
        setupNavigationBar()
        viewModel.getUserInfo()
        viewModel.fetchPokemonTypes()
        showLoadingPopup()
    }
    
    func setup(){
        self.collectionView.register(type: PokemonTypeCollectionViewCell.self)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.backgroundColor = .white
        
        self.tableView.register(PokemonTableViewHeaderView.nib, forHeaderFooterViewReuseIdentifier: PokemonTableViewHeaderView.reuseIdentifier)
        self.tableView.register(type: PokemonTableViewCell.self)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.backgroundColor = .white
    }
    
    init(coordinator: MainCoordinator) {
        self.coordinator = coordinator
        self.viewModel = HomeViewModel()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.pokemonTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PokemonTypeCollectionViewCell = collectionView.dequeueReusableCell(for: indexPath)
        cell.pokemonType = viewModel.pokemonTypes[indexPath.row]
        cell.isSelectedType = viewModel.pokemonTypes[indexPath.row].name == self.selectedType
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width * 0.24, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let type = viewModel.pokemonTypes[indexPath.row].name
        if type != self.selectedType {
            self.selectedType = viewModel.pokemonTypes[indexPath.row].name
            viewModel.fetchPokemons(with: self.selectedType)
            showLoadingPopup()
            collectionView.reloadData()
        }
    }
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.separatorStyle =  viewModel.pokemons.count == 0 ? .none : .singleLine
        return viewModel.pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PokemonTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.pokemon = viewModel.pokemons[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView,
                            viewForHeaderInSection section: Int) -> UIView? {
        guard let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: PokemonTableViewHeaderView.reuseIdentifier) as? PokemonTableViewHeaderView
        else { return nil }
        view.delegate = self
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}

extension HomeViewController: PokemonTableViewHeaderViewDelegate {
    
    func nameFilterClicked(type: FilterType) {
        viewModel.filterPokemons(with: type)
    }
}

extension HomeViewController: HomeViewModelDelegate {
    
    func didUpdateFavoriteType(type: String) {
        self.navigationController?.navigationBar.isUserInteractionEnabled = true
        selectPokemonTypeView.removeFromSuperview()
        self.selectedType = type
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
        showLoadingPopup()
        viewModel.fetchPokemons(with: self.selectedType)
    }
    
    
    func didUpdatePokemons() {
        hideLoadingPopup()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func didGetPokemonTypes() {
        hideLoadingPopup()
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func didGetUserInfo(user: User) {
        self.selectedType = user.favoritePokemonType
        viewModel.fetchPokemons(with: user.favoritePokemonType)
    }
    
    func logout() {
        coordinator.logout()
    }
    
    func failed(message: String) {
        present(message: message)
    }
    
    
}
