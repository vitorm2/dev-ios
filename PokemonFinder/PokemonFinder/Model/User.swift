//
//  User.swift
//  PokemonFinder
//
//  Created by Vitor Demenighi on 15/11/20.
//

import Foundation

class User: Codable {
    
    let userName: String
    var favoritePokemonType: String
    
    init(userName: String, favoritePokemonType: String) {
        self.userName = userName
        self.favoritePokemonType = favoritePokemonType
    }
}

